from django.urls import path
from . import views

app_name = "polls"

urlpatterns = [
    path('', views.index, name='index'),
    path('mudaKu/', views.mudaKu, name='mudaKu'),
    path('blueKu/', views.blueKu, name='blueKu'),
    path('time/', views.time, name='time'),
    path('time/<str:time>', views.time, name='time'),
]