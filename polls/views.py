import datetime
from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, "index.html", {'static' : 'true', 'color' : 'none'})
def mudaKu(request):
    color = 'muda'
    return render(request, "mudaKu.html", {'color' : color})
def blueKu(request):
    color = 'blue'
    return render(request, "blueKu.html", {'color' : color})
def time(request, time = '0'):
    time = int(time)
    time = time + 7
    TIME_ZONE = datetime.datetime.now() + datetime.timedelta(hours = time)
    return render(request, "time.html", {'time' : TIME_ZONE})
