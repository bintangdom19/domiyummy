"""introppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from story_3 import views
# from polls import views
# from testform1 import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home_view),
    path('some/', views.some_view),
    path('oke/', views.oke_view),
    path('apa/', views.apa_view),
    path('course/', include('testform1.urls')),
    path('polls/' , include('polls.urls')),  
    path('event/' , include('story6.urls')), 
     # path('index/', views.index),
    # path('show/', views.show),
    # path('showAdmin/', views.showAdmin),
    # path('create/', views.create),
    # path('indexAdmin/', views.indexAdmin),

]
