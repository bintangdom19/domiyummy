from django.urls import path
from . import views

app_name = ["story_3"]
urlpatterns = [
    path('', views.home_view, name="home_url"),
]