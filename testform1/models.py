from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class MataKuliah(models.Model):
    order = models.AutoField(primary_key=True)
    namaMatkul = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    jumlahSKS = models.IntegerField()
    deskripsiMatkul = models.TextField()
    waktu = models.CharField(max_length=50)
    semester = models.CharField(max_length=100)

    def __str__(self):
        return self.namaMatkul
    
    def snippet(self):
        return self.deskripsiMatkul[0:150] + "..."

class Tugas(models.Model):
    namaTugas = models.CharField(max_length=100)
    tanggal = models.DateTimeField(auto_now=False, auto_now_add=False)
    deskripsiTugas = models.TextField()
    matakuliah = models.ForeignKey(MataKuliah, on_delete=models.CASCADE)

    def __str__(self):
        return self.namaTugas